const production = !process.env.ROLLUP_WATCH;

module.exports = {
  variants: {
    extends: {
      borderWidth: ["hover", "focus"],
      backgroundColor: ["active"],
    },
  },
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  plugins: [],
  purge: {
    content: ["./src/**/*.svelte", "./src/**/*.html", "./src/**/*.css"],
    enabled: production,
  },
};
